nastyabatanina:~/workspace/pr2 (master) $ valgrind --leak-check=full ./src/db
==3042== Memcheck, a memory error detector
==3042== Copyright (C) 2002-2013, and GNU GPL'd, by Julian Seward et al.
==3042== Using Valgrind-3.10.1 and LibVEX; rerun with -h for copyright info
==3042== Command: ./bin/db.out
==3042== 

nastyabatanina:~/workspace/pr2 (master) $ ./src/db

Glu PR2. Lesson database.

add    Create a lesson
ls     Browse lessons
save   Export lessons into JSON file
load   Import lessons from JSON file
help   Show this message

Type 'exit' to quit
> add

Creating lesson.
Enter teacher: Glu
Enter auditory: 103a
Enter start date(dd.mm.yyyy): 26.04.2017
Enter start time(h:mm): 8:30
Enter end date(dd.mm.yyyy): 26.04.2017
Enter end time(h:mm): 10:00
Is data correct? (y/n): y
Successfully created lesson.

> ls

Printing 1 lesson(s):

1. Teacher: Glu
Auditory: 103a
Start: 26.04.2017 8:30
End: 26.04.2017 10:0

> save

File name (path): test.json
Exported 1 lesson(s).

> load

File name (path): test.json
Imported 1 lesson(s).

> help


add    Create a lesson
ls     Browse lessons
save   Export lessons into JSON file
load   Import lessons from JSON file
help   Show this message

Type 'exit' to quit
> exit

==3042== 
==3042== HEAP SUMMARY:
==3042==     in use at exit: 73,080 bytes in 10 blocks
==3042==   total heap usage: 215 allocs, 205 frees, 99,671 bytes allocated
==3042== 
==3042== 174 (96 direct, 78 indirect) bytes in 1 blocks are definitely lost in loss record 5 of 7
==3042==    at 0x4C2B0E0: operator new(unsigned long) (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==3042==    by 0x403E65: launch_create_student_dialog() (student.cpp:21)
==3042==    by 0x41471A: main (main.cpp:64)
==3042== 
==3042== 202 (96 direct, 106 indirect) bytes in 1 blocks are definitely lost in loss record 6 of 7
==3042==    at 0x4C2B0E0: operator new(unsigned long) (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==3042==    by 0x405662: load_file() (student.cpp:202)
==3042==    by 0x414755: main (main.cpp:72)
==3042== 
==3042== LEAK SUMMARY:
==3042==    definitely lost: 192 bytes in 2 blocks
==3042==    indirectly lost: 184 bytes in 7 blocks
==3042==      possibly lost: 0 bytes in 0 blocks
==3042==    still reachable: 72,704 bytes in 1 blocks
==3042==         suppressed: 0 bytes in 0 blocks
==3042== Reachable blocks (those to which a pointer was found) are not shown.
==3042== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==3042== 
==3042== For counts of detected and suppressed errors, rerun with: -v
==3042== ERROR SUMMARY: 2 errors from 2 contexts (suppressed: 0 from 0)
nastyabatanina:~/workspace/pr2 (master) $